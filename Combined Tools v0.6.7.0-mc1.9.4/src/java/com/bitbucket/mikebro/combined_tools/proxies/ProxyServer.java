package com.bitbucket.mikebro.combined_tools.proxies;

import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

/** stores and executes Server side initialization events */
public class ProxyServer extends ProxyCommon {

	/** Server pre initialization events */
	@Override
	public void preInit(FMLPreInitializationEvent e) {
		super.preInit(e);
	}

	/** Server main initialization events */
	@Override
	public void init(FMLInitializationEvent e) {
		super.init(e);
	}

	/** Server post initialization events */
	@Override
	public void postInit(FMLPostInitializationEvent e) {
		super.postInit(e);
	}
}
