package com.bitbucket.mikebro.combined_tools.crafting;

import com.bitbucket.mikebro.combined_tools.items.ModItems;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModCrafting {

	public static void initCrafting() {
		GameRegistry.addRecipe(new ItemStack(ModItems.woodenAxehoe), "AWB", " I ", " I ", 'A', Items.WOODEN_AXE, 'B',
				Items.WOODEN_HOE, 'W', Blocks.PLANKS, 'I', Items.STICK);
		GameRegistry.addRecipe(new ItemStack(ModItems.stoneAxehoe), "AWB", " I ", " I ", 'A', Items.STONE_AXE, 'B',
				Items.STONE_HOE, 'W', Blocks.COBBLESTONE, 'I', Items.STICK);
		GameRegistry.addRecipe(new ItemStack(ModItems.ironAxehoe), "AWB", " I ", " I ", 'A', Items.IRON_AXE, 'B',
				Items.IRON_HOE, 'W', Items.IRON_INGOT, 'I', Items.STICK);
		GameRegistry.addRecipe(new ItemStack(ModItems.diamondAxehoe), "AWB", " I ", " I ", 'A', Items.DIAMOND_AXE, 'B',
				Items.DIAMOND_HOE, 'W', Items.DIAMOND, 'I', Items.STICK);
		GameRegistry.addRecipe(new ItemStack(ModItems.goldAxehoe), "AWB", " I ", " I ", 'A', Items.GOLDEN_AXE, 'B',
				Items.GOLDEN_HOE, 'W', Items.GOLD_INGOT, 'I', Items.STICK);

		GameRegistry.addRecipe(new ItemStack(ModItems.woodenAxepick), "AIB", " I ", " I ", 'A', Items.WOODEN_AXE, 'B',
				Items.WOODEN_PICKAXE, 'I', Items.STICK);
		GameRegistry.addRecipe(new ItemStack(ModItems.stoneAxepick), "AIB", " I ", " I ", 'A', Items.STONE_AXE, 'B',
				Items.STONE_PICKAXE, 'I', Items.STICK);
		GameRegistry.addRecipe(new ItemStack(ModItems.ironAxepick), "AIB", " I ", " I ", 'A', Items.IRON_AXE, 'B',
				Items.IRON_PICKAXE, 'I', Items.STICK);
		GameRegistry.addRecipe(new ItemStack(ModItems.diamondAxepick), "AIB", " I ", " I ", 'A', Items.DIAMOND_AXE, 'B',
				Items.DIAMOND_PICKAXE, 'I', Items.STICK);
		GameRegistry.addRecipe(new ItemStack(ModItems.goldAxepick), "AIB", " I ", " I ", 'A', Items.GOLDEN_AXE, 'B',
				Items.GOLDEN_PICKAXE, 'I', Items.STICK);

		GameRegistry.addRecipe(new ItemStack(ModItems.woodenAxeshovel), "AWB", " W ", " I ", 'A', Items.WOODEN_AXE, 'B',
				Items.WOODEN_SHOVEL, 'W', Blocks.PLANKS, 'I', Items.STICK);
		GameRegistry.addRecipe(new ItemStack(ModItems.stoneAxeshovel), "AWB", " W ", " I ", 'A', Items.STONE_AXE, 'B',
				Items.STONE_SHOVEL, 'W', Blocks.COBBLESTONE, 'I', Items.STICK);
		GameRegistry.addRecipe(new ItemStack(ModItems.ironAxeshovel), "AWB", " W ", " I ", 'A', Items.IRON_AXE, 'B',
				Items.IRON_SHOVEL, 'W', Items.IRON_INGOT, 'I', Items.STICK);
		GameRegistry.addRecipe(new ItemStack(ModItems.diamondAxeshovel), "AWB", " W ", " I ", 'A', Items.DIAMOND_AXE,
				'B', Items.DIAMOND_SHOVEL, 'W', Items.DIAMOND, 'I', Items.STICK);
		GameRegistry.addRecipe(new ItemStack(ModItems.goldAxeshovel), "AWB", " W ", " I ", 'A', Items.GOLDEN_AXE, 'B',
				Items.GOLDEN_SHOVEL, 'W', Items.GOLD_INGOT, 'I', Items.STICK);

		GameRegistry.addRecipe(new ItemStack(ModItems.woodenGatherer), "ABC", "WIW", " I ", 'A', Items.WOODEN_AXE, 'B',
				Items.WOODEN_PICKAXE, 'C', Items.WOODEN_SHOVEL, 'W', Blocks.PLANKS, 'I', Items.STICK);
		GameRegistry.addRecipe(new ItemStack(ModItems.stoneGatherer), "ABC", "WIW", " I ", 'A', Items.STONE_AXE, 'B',
				Items.STONE_PICKAXE, 'C', Items.STONE_SHOVEL, 'W', Blocks.COBBLESTONE, 'I', Items.STICK);
		GameRegistry.addRecipe(new ItemStack(ModItems.ironGatherer), "ABC", "WIW", " I ", 'A', Items.IRON_AXE, 'B',
				Items.IRON_PICKAXE, 'C', Items.IRON_SHOVEL, 'W', Items.IRON_INGOT, 'I', Items.STICK);
		GameRegistry.addRecipe(new ItemStack(ModItems.diamondGatherer), "ABC", "WIW", " I ", 'A', Items.DIAMOND_AXE,
				'B', Items.DIAMOND_PICKAXE, 'C', Items.DIAMOND_SHOVEL, 'W', Items.DIAMOND, 'I', Items.STICK);
		GameRegistry.addRecipe(new ItemStack(ModItems.goldGatherer), "ABC", "WIW", " I ", 'A', Items.GOLDEN_AXE, 'B',
				Items.GOLDEN_PICKAXE, 'C', Items.GOLDEN_SHOVEL, 'W', Items.GOLD_INGOT, 'I', Items.STICK);

		GameRegistry.addRecipe(new ItemStack(ModItems.woodenGatherer), "ABC", "WIW", " I ", 'A', Items.WOODEN_PICKAXE,
				'B', Items.WOODEN_AXE, 'C', Items.WOODEN_SHOVEL, 'W', Blocks.PLANKS, 'I', Items.STICK);
		GameRegistry.addRecipe(new ItemStack(ModItems.stoneGatherer), "ABC", "WIW", " I ", 'A', Items.STONE_PICKAXE,
				'B', Items.STONE_AXE, 'C', Items.STONE_SHOVEL, 'W', Blocks.COBBLESTONE, 'I', Items.STICK);
		GameRegistry.addRecipe(new ItemStack(ModItems.ironGatherer), "ABC", "WIW", " I ", 'A', Items.IRON_PICKAXE, 'B',
				Items.IRON_AXE, 'C', Items.IRON_SHOVEL, 'W', Items.IRON_INGOT, 'I', Items.STICK);
		GameRegistry.addRecipe(new ItemStack(ModItems.diamondGatherer), "ABC", "WIW", " I ", 'A', Items.DIAMOND_PICKAXE,
				'B', Items.DIAMOND_AXE, 'C', Items.DIAMOND_SHOVEL, 'W', Items.DIAMOND, 'I', Items.STICK);
		GameRegistry.addRecipe(new ItemStack(ModItems.goldGatherer), "ABC", "WIW", " I ", 'A', Items.GOLDEN_PICKAXE,
				'B', Items.GOLDEN_AXE, 'C', Items.GOLDEN_SHOVEL, 'W', Items.GOLD_INGOT, 'I', Items.STICK);

		GameRegistry.addRecipe(new ItemStack(ModItems.woodenGatherer), "ABC", "WIW", " I ", 'A', Items.WOODEN_AXE, 'B',
				Items.WOODEN_SHOVEL, 'C', Items.WOODEN_PICKAXE, 'W', Blocks.PLANKS, 'I', Items.STICK);
		GameRegistry.addRecipe(new ItemStack(ModItems.stoneGatherer), "ABC", "WIW", " I ", 'A', Items.STONE_AXE, 'B',
				Items.STONE_SHOVEL, 'C', Items.STONE_PICKAXE, 'W', Blocks.COBBLESTONE, 'I', Items.STICK);
		GameRegistry.addRecipe(new ItemStack(ModItems.ironGatherer), "ABC", "WIW", " I ", 'A', Items.IRON_AXE, 'B',
				Items.IRON_SHOVEL, 'C', Items.IRON_PICKAXE, 'W', Items.IRON_INGOT, 'I', Items.STICK);
		GameRegistry.addRecipe(new ItemStack(ModItems.diamondGatherer), "ABC", "WIW", " I ", 'A', Items.DIAMOND_AXE,
				'B', Items.DIAMOND_SHOVEL, 'C', Items.DIAMOND_PICKAXE, 'W', Items.DIAMOND, 'I', Items.STICK);
		GameRegistry.addRecipe(new ItemStack(ModItems.goldGatherer), "ABC", "WIW", " I ", 'A', Items.GOLDEN_AXE, 'B',
				Items.GOLDEN_SHOVEL, 'C', Items.GOLDEN_PICKAXE, 'W', Items.GOLD_INGOT, 'I', Items.STICK);

		GameRegistry.addRecipe(new ItemStack(ModItems.woodenPickhoe), "AWB", " I ", " I ", 'A', Items.WOODEN_PICKAXE,
				'B', Items.WOODEN_HOE, 'W', Blocks.PLANKS, 'I', Items.STICK);
		GameRegistry.addRecipe(new ItemStack(ModItems.stonePickhoe), "AWB", " I ", " I ", 'A', Items.STONE_PICKAXE, 'B',
				Items.STONE_HOE, 'W', Blocks.COBBLESTONE, 'I', Items.STICK);
		GameRegistry.addRecipe(new ItemStack(ModItems.ironPickhoe), "AWB", " I ", " I ", 'A', Items.IRON_PICKAXE, 'B',
				Items.IRON_HOE, 'W', Items.IRON_INGOT, 'I', Items.STICK);
		GameRegistry.addRecipe(new ItemStack(ModItems.diamondPickhoe), "AWB", " I ", " I ", 'A', Items.DIAMOND_PICKAXE,
				'B', Items.DIAMOND_HOE, 'W', Items.DIAMOND, 'I', Items.STICK);
		GameRegistry.addRecipe(new ItemStack(ModItems.goldPickhoe), "AWB", " I ", " I ", 'A', Items.GOLDEN_PICKAXE, 'B',
				Items.GOLDEN_HOE, 'W', Items.GOLD_INGOT, 'I', Items.STICK);

		GameRegistry.addRecipe(new ItemStack(ModItems.woodenPickshovel), "AWB", " W ", " I ", 'A', Items.WOODEN_PICKAXE,
				'B', Items.WOODEN_SHOVEL, 'W', Blocks.PLANKS, 'I', Items.STICK);
		GameRegistry.addRecipe(new ItemStack(ModItems.stonePickshovel), "AWB", " W ", " I ", 'A', Items.STONE_PICKAXE,
				'B', Items.STONE_SHOVEL, 'W', Blocks.COBBLESTONE, 'I', Items.STICK);
		GameRegistry.addRecipe(new ItemStack(ModItems.ironPickshovel), "AWB", " W ", " I ", 'A', Items.IRON_PICKAXE,
				'B', Items.IRON_SHOVEL, 'W', Items.IRON_INGOT, 'I', Items.STICK);
		GameRegistry.addRecipe(new ItemStack(ModItems.diamondPickshovel), "AWB", " W ", " I ", 'A',
				Items.DIAMOND_PICKAXE, 'B', Items.DIAMOND_SHOVEL, 'W', Items.DIAMOND, 'I', Items.STICK);
		GameRegistry.addRecipe(new ItemStack(ModItems.goldPickshovel), "AWB", " W ", " I ", 'A', Items.GOLDEN_PICKAXE,
				'B', Items.GOLDEN_SHOVEL, 'W', Items.GOLD_INGOT, 'I', Items.STICK);
	}
}
