package com.bitbucket.mikebro.combined_tools;

/** is only used to store/access mod information*/
public class CombinedToolsReference {
	/** contains the internal ID of the mod*/
	public static final String ID = "combined_tools";
	
	/** contains the full/writen out/shown name of the Mod*/
	public static final String NAME = "Combined Tools MC1.9.4";
	
	/** contains the current version of the Mod*/
	public static final String VER = "v0.6.7.0-mc1.9.4";
}
