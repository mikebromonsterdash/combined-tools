package com.bitbucket.mikebro.combined_tools.items;

import net.minecraft.item.ItemHoe;

/** Class for custom hoes. */
public class ModItemToolHoe extends ItemHoe {

	/** Constructor for a custom hoe. */
	protected ModItemToolHoe(String unlocalizedName, ToolMaterial material) {
		super(material);

		this.setUnlocalizedName(unlocalizedName);
		this.setRegistryName(unlocalizedName);
	}
}
