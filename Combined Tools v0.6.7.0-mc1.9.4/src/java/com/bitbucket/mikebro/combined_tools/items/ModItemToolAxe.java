package com.bitbucket.mikebro.combined_tools.items;

import java.util.Set;

import com.google.common.collect.Sets;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTool;

/** Class for custom axes */
public class ModItemToolAxe extends ItemTool {

	public static final Set<Block> EFFECTIVE_ON = Sets.newHashSet(new Block[] { Blocks.PLANKS, Blocks.BOOKSHELF,
			Blocks.LOG, Blocks.LOG2, Blocks.CHEST, Blocks.PUMPKIN, Blocks.LIT_PUMPKIN, Blocks.MELON_BLOCK,
			Blocks.LADDER, Blocks.WOODEN_BUTTON, Blocks.WOODEN_PRESSURE_PLATE });

	public static final float[] ATTACK_DAMAGES = new float[] { 6.0F, 8.0F, 8.0F, 8.0F, 6.0F };

	public static final float[] ATTACK_SPEEDS = new float[] { -3.2F, -3.2F, -3.1F, -3.0F, -3.0F };

	/**
	 * Constructor for a custom axe. [damage] determines damage done by hitting
	 * mobs (is added to barehand damage, which is 1.0F). [speed] determines
	 * attack speed (is usually negative and is added to barehand speed, which
	 * is 4.0F).
	 */
	protected ModItemToolAxe(String unlocalizedName, ToolMaterial material, float damage, float speed) {
		super(material, EFFECTIVE_ON);
		this.damageVsEntity = damage;
		this.attackSpeed = speed;
		this.setUnlocalizedName(unlocalizedName);
		this.setRegistryName(unlocalizedName);
	}

	/**
	 * Lazy constructor for a custom axe, grants damage and speed according to
	 * the harvestLevel of the material. If harvestLevel is not regular (i.e.
	 * not 0 to 3) assigns 7.0F and -3.1F respectively.
	 */
	protected ModItemToolAxe(String unlocalizedName, ToolMaterial material) {
		super(material, EFFECTIVE_ON);
		if (material.getHarvestLevel() == 0 || material.getHarvestLevel() == 1 || material.getHarvestLevel() == 2
				|| material.getHarvestLevel() == 3) {
			this.damageVsEntity = ATTACK_DAMAGES[material.getHarvestLevel()];
			if (material.getEnchantability() == 22)
				this.attackSpeed = ATTACK_SPEEDS[4];
			else
				this.attackSpeed = ATTACK_SPEEDS[material.getHarvestLevel()];
		} else {
			this.damageVsEntity = 7.0F;
			this.attackSpeed = -3.1F;
		}

		this.setUnlocalizedName(unlocalizedName);
		this.setRegistryName(unlocalizedName);
	}

	public float getStrVsBlock(ItemStack stack, IBlockState state) {
		Material material = state.getMaterial();
		return material != Material.WOOD && material != Material.PLANTS && material != Material.VINE
				? super.getStrVsBlock(stack, state) : this.efficiencyOnProperMaterial;
	} 
}
