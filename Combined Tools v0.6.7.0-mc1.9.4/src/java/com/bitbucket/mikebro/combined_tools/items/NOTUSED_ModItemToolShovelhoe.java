package com.bitbucket.mikebro.combined_tools.items;

import net.minecraft.block.Block;
import net.minecraft.block.BlockDirt;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemSpade;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

// TO DO: decide whether to use Shovel or Hoe behavior in onItemUse;
//		  decide whether it would be possible/sensible to use both
/** Class for custom "Shovelhoes", a combination of a spade and hoe. */
public class NOTUSED_ModItemToolShovelhoe extends ItemSpade{
	
	protected NOTUSED_ModItemToolShovelhoe(String unlocalizedName, ToolMaterial material) {
		super(material);

		this.setUnlocalizedName(unlocalizedName);
		this.setRegistryName(unlocalizedName);

	}

	/**
	 * Called when a Block is right-clicked with this Item
	 */
	
	@SuppressWarnings("incomplete-switch")
	@Override
	public EnumActionResult onItemUse(ItemStack stack, EntityPlayer playerIn, World worldIn, BlockPos pos,
			EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
		if (worldIn.getBlockState(pos).getBlock() == Blocks.GRASS) {
			return super.onItemUse(stack, playerIn, worldIn, pos, hand, facing, hitX, hitY, hitZ);
		} else {
			if (!playerIn.canPlayerEdit(pos.offset(facing), facing, stack)) {
				return EnumActionResult.FAIL;
			} else {
				int hook = net.minecraftforge.event.ForgeEventFactory.onHoeUse(stack, playerIn, worldIn, pos);
				if (hook != 0)
					return hook > 0 ? EnumActionResult.SUCCESS : EnumActionResult.FAIL;

				IBlockState iblockstate = worldIn.getBlockState(pos);
				Block block = iblockstate.getBlock();

				if (facing != EnumFacing.DOWN && worldIn.isAirBlock(pos.up())) {
					if (block == Blocks.GRASS || block == Blocks.GRASS_PATH) {
						this.func_185071_a(stack, playerIn, worldIn, pos, Blocks.FARMLAND.getDefaultState());
						return EnumActionResult.SUCCESS;
					}

					if (block == Blocks.DIRT) {
						switch ((BlockDirt.DirtType) iblockstate.getValue(BlockDirt.VARIANT)) {
						case DIRT:
							this.func_185071_a(stack, playerIn, worldIn, pos, Blocks.FARMLAND.getDefaultState());
							return EnumActionResult.SUCCESS;
						case COARSE_DIRT:
							this.func_185071_a(stack, playerIn, worldIn, pos, Blocks.DIRT.getDefaultState()
									.withProperty(BlockDirt.VARIANT, BlockDirt.DirtType.DIRT));
							return EnumActionResult.SUCCESS;
						}
					}
				}

				return EnumActionResult.PASS;
			}
		}
	}

	protected void func_185071_a(ItemStack stack, EntityPlayer player, World worldIn, BlockPos pos, IBlockState state) {
		worldIn.playSound(player, pos, SoundEvents.ITEM_HOE_TILL, SoundCategory.BLOCKS, 1.0F, 1.0F);

		if (!worldIn.isRemote) {
			worldIn.setBlockState(pos, state, 11);
			stack.damageItem(1, player);
		}
	}
}
