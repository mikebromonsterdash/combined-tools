package com.bitbucket.mikebro.combined_tools.items;

import java.util.Set;

import com.google.common.collect.Sets;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;

/** Class for custom "Axepicks", a combination of an axe and pickaxe */
public class ModItemToolAxepick extends ModItemToolPickaxe {

	public static Set<Block> effectiveAgainst = Sets.newHashSet(new Block[] { Blocks.PLANKS, Blocks.BOOKSHELF,
			Blocks.LOG, Blocks.LOG2, Blocks.CHEST, Blocks.PUMPKIN, Blocks.LIT_PUMPKIN, Blocks.MELON_BLOCK,
			Blocks.LADDER, Blocks.WOODEN_BUTTON, Blocks.WOODEN_PRESSURE_PLATE });

	/**
	 * Constructor for a custom Axepick. [damage] determines damage done by
	 * hitting mobs (is added to barehand damage, which is 1.0F). [speed]
	 * determines attack speed (is usually negative and is added to barehand
	 * speed, which is 4.0F).
	 */
	protected ModItemToolAxepick(String unlocalizedName, ToolMaterial material, float damage, float speed) {
		super(unlocalizedName, material);
		this.damageVsEntity = damage;
		this.attackSpeed = speed;
	}

	/**
	 * Lazy constructor for a custom Axepick. Damage & speed are determined by
	 * the material.getHarvestLevel() value. To differentiate gold from wood
	 * material.getEnchantability is consulted (is 15 for wood and 22 for gold).
	 * Is the harvestLevel irregular (i.e. not 0 - 3) a value is calculated
	 * using fixed and material.getDamageVsEntity() values.
	 */
	protected ModItemToolAxepick(String unlocalizedName, ToolMaterial material) {
		super(unlocalizedName, material);
		if (material.getHarvestLevel() == 0 || material.getHarvestLevel() == 1 || material.getHarvestLevel() == 2
				|| material.getHarvestLevel() == 3) {
			this.damageVsEntity = (ModItemToolAxe.ATTACK_DAMAGES[material.getHarvestLevel()] + 1.0f
					+ material.getDamageVsEntity()) / 2;
			if (material.getEnchantability() == 22)
				this.attackSpeed = (ModItemToolAxe.ATTACK_SPEEDS[4] - 2.8f) / 2;
			else
				this.attackSpeed = (ModItemToolAxe.ATTACK_SPEEDS[material.getHarvestLevel()] - 2.8f) / 2;
		} else {
			this.damageVsEntity = (7.0F + 1.0f + material.getDamageVsEntity()) / 2;
			this.attackSpeed = (-3.1F - 2.8f) / 2;
		}
	}

	@Override
	public float getStrVsBlock(ItemStack stack, IBlockState state) {
		Block block = state.getBlock();
		if (state.getMaterial() == Material.WOOD || state.getMaterial() == Material.PLANTS
				|| state.getMaterial() == Material.VINE)
			return this.efficiencyOnProperMaterial;

		return effectiveAgainst.contains(block) ? this.efficiencyOnProperMaterial : super.getStrVsBlock(stack, state);
	}

}
