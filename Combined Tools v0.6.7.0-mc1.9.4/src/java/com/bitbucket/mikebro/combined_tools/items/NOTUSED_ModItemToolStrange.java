package com.bitbucket.mikebro.combined_tools.items;

import net.minecraft.block.Block;
import net.minecraft.block.BlockDirt;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;



/**
 * Class for custom "Strage Tridents", a combination of an axe, pickaxe and hoe.
 */
// this is a difficult to visualize tool; planning on dropping it
public class NOTUSED_ModItemToolStrange extends ModItemToolAxepick {

	/**
	 * Constructor for a custom Strage Trident. [damage] determines damage done
	 * by hitting mobs (is added to barehand damage, which is 1.0F). [speed]
	 * determines attack speed (is usually negative and is added to barehand
	 * speed, which is 4.0F).
	 */
	protected NOTUSED_ModItemToolStrange(String unlocalizedName, ToolMaterial material, float damage, float speed) {
		super(unlocalizedName, material, damage, speed);
	}

	/**
	 * Lazy constructor for a custom Strage Trident. Damage & speed are
	 * determined by the material.getHarvestLevel() value. To differentiate gold
	 * from wood material.getEnchantability is consulted (is 15 for wood and 22
	 * for gold). Is the harvestLevel irregular (i.e. not 0 - 3) a value is
	 * calculated using fixed and material.getDamageVsEntity() values.
	 */
	protected NOTUSED_ModItemToolStrange(String unlocalizedName, ToolMaterial material) {
		super(unlocalizedName, material, 3.0f, -3.0f);
		if (material.getHarvestLevel() == 0 || material.getHarvestLevel() == 1 || material.getHarvestLevel() == 2
				|| material.getHarvestLevel() == 3) {
			this.damageVsEntity = (ModItemToolAxe.ATTACK_DAMAGES[material.getHarvestLevel()]
					+ 2 * (1.0f + material.getDamageVsEntity())) / 3;
			if (material.getEnchantability() == 22)
				this.attackSpeed = (ModItemToolAxe.ATTACK_SPEEDS[4] - 2.8f) / 2;
			else
				this.attackSpeed = (ModItemToolAxe.ATTACK_SPEEDS[material.getHarvestLevel()] - 2.8f) / 2;
		} else {
			this.damageVsEntity = (7.0F + 1.0f + material.getDamageVsEntity()) / 2;
			this.attackSpeed = (-3.1F - 2.8f) / 2;
		}
	}

	/** Called when a Block is right-clicked with this Item. */
	@SuppressWarnings("incomplete-switch")
	public EnumActionResult onItemUse(ItemStack stack, EntityPlayer playerIn, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
    {
        if (!playerIn.canPlayerEdit(pos.offset(facing), facing, stack))
        {
            return EnumActionResult.FAIL;
        }
        else
        {
            int hook = net.minecraftforge.event.ForgeEventFactory.onHoeUse(stack, playerIn, worldIn, pos);
            if (hook != 0) return hook > 0 ? EnumActionResult.SUCCESS : EnumActionResult.FAIL;

            IBlockState iblockstate = worldIn.getBlockState(pos);
            Block block = iblockstate.getBlock();

            if (facing != EnumFacing.DOWN && worldIn.isAirBlock(pos.up()))
            {
                if (block == Blocks.GRASS || block == Blocks.GRASS_PATH)
                {
                    this.setBlock(stack, playerIn, worldIn, pos, Blocks.FARMLAND.getDefaultState());
                    return EnumActionResult.SUCCESS;
                }

                if (block == Blocks.DIRT)
                {
                    switch ((BlockDirt.DirtType)iblockstate.getValue(BlockDirt.VARIANT))
                    {
                        case DIRT:
                            this.setBlock(stack, playerIn, worldIn, pos, Blocks.FARMLAND.getDefaultState());
                            return EnumActionResult.SUCCESS;
                        case COARSE_DIRT:
                            this.setBlock(stack, playerIn, worldIn, pos, Blocks.DIRT.getDefaultState().withProperty(BlockDirt.VARIANT, BlockDirt.DirtType.DIRT));
                            return EnumActionResult.SUCCESS;
                    }
                }
            }

            return EnumActionResult.PASS;
        }
    }

	protected void setBlock(ItemStack stack, EntityPlayer player, World worldIn, BlockPos pos, IBlockState state)
    {
        worldIn.playSound(player, pos, SoundEvents.ITEM_HOE_TILL, SoundCategory.BLOCKS, 1.0F, 1.0F);

        if (!worldIn.isRemote)
        {
            worldIn.setBlockState(pos, state, 11);
            stack.damageItem(1, player);
        }
    }
}
