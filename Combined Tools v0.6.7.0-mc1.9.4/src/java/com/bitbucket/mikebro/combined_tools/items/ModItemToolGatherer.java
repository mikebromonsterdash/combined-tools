package com.bitbucket.mikebro.combined_tools.items;

import java.util.Set;

import com.google.common.collect.Sets;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 * Class for custom "Gatherer's Tridents", a combination of an axe, pickaxe and
 * spade.
 */
public class ModItemToolGatherer extends ModItemToolAxepick {

	private static Set<Block> effectiveAgainst = Sets.newHashSet(
			new Block[] { Blocks.CLAY, Blocks.DIRT, Blocks.FARMLAND, Blocks.GRASS, Blocks.GRAVEL, Blocks.MYCELIUM,
					Blocks.SAND, Blocks.SNOW, Blocks.SNOW_LAYER, Blocks.SOUL_SAND, Blocks.GRASS_PATH });

	/**
	 * Constructor for a custom Gatherer's Trident. [damage] determines damage
	 * done by hitting mobs (is added to barehand damage, which is 1.0F).
	 * [speed] determines attack speed (is usually negative and is added to
	 * barehand speed, which is 4.0F).
	 */
	protected ModItemToolGatherer(String unlocalizedName, ToolMaterial material, float damage, float speed) {
		super(unlocalizedName, material, damage, speed);
	}

	/**
	 * Lazy constructor for a custom Gatherer's Trident. Damage & speed are
	 * determined by the material.getHarvestLevel() value. To differentiate gold
	 * from wood material.getEnchantability is consulted (is 15 for wood and 22
	 * for gold). Is the harvestLevel irregular (i.e. not 0 - 3) a value is
	 * calculated using fixed and material.getDamageVsEntity() values.
	 */
	protected ModItemToolGatherer(String unlocalizedName, ToolMaterial material) {
		super(unlocalizedName, material, 3.0f, -3.0f);
		if (material.getHarvestLevel() == 0 || material.getHarvestLevel() == 1 || material.getHarvestLevel() == 2
				|| material.getHarvestLevel() == 3) {
			this.damageVsEntity = (ModItemToolAxe.ATTACK_DAMAGES[material.getHarvestLevel()] + 1.0f + 1.5f
					+ 2 * material.getDamageVsEntity()) / 3;
			if (material.getEnchantability() == 22)
				this.attackSpeed = (ModItemToolAxe.ATTACK_SPEEDS[4] - 2.8f - 3.0f) / 3;
			else
				this.attackSpeed = (ModItemToolAxe.ATTACK_SPEEDS[material.getHarvestLevel()] - 2.8f - 3.0f) / 3;
		} else {
			this.damageVsEntity = (7.0F + 1.0f + 1.5f + material.getDamageVsEntity() * 2) / 3;
			this.attackSpeed = (-3.1F - 2.8f - 3.0f) / 3;
		}
	}

	/**
	 * Check whether this Item can harvest the given Block
	 */
	@Override
	public boolean canHarvestBlock(IBlockState blockIn) {
		Block block = blockIn.getBlock();
		return effectiveAgainst.contains(block) ? true : super.canHarvestBlock(blockIn);
	}

	@Override
	public float getStrVsBlock(ItemStack stack, IBlockState state) {
		Block block = state.getBlock();
		return effectiveAgainst.contains(block) ? this.efficiencyOnProperMaterial : super.getStrVsBlock(stack, state);
	}

	/**
	 * Called when a Block is right-clicked with this Item
	 */
	public EnumActionResult onItemUse(ItemStack stack, EntityPlayer playerIn, World worldIn, BlockPos pos,
			EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
		if (!playerIn.canPlayerEdit(pos.offset(facing), facing, stack)) {
			return EnumActionResult.FAIL;
		} else {
			IBlockState iblockstate = worldIn.getBlockState(pos);
			Block block = iblockstate.getBlock();

			if (facing != EnumFacing.DOWN && worldIn.getBlockState(pos.up()).getMaterial() == Material.AIR
					&& block == Blocks.GRASS) {
				IBlockState iblockstate1 = Blocks.GRASS_PATH.getDefaultState();
				worldIn.playSound(playerIn, pos, SoundEvents.ITEM_SHOVEL_FLATTEN, SoundCategory.BLOCKS, 1.0F, 1.0F);

				if (!worldIn.isRemote) {
					worldIn.setBlockState(pos, iblockstate1, 11);
					stack.damageItem(1, playerIn);
				}

				return EnumActionResult.SUCCESS;
			} else {
				return EnumActionResult.PASS;
			}
		}
	}
}
