package com.bitbucket.mikebro.combined_tools.client.renderer;

import com.bitbucket.mikebro.combined_tools.items.ModItems;

import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;

public final class RenderItems {

	/** Registers a texture to a given registered Item */
	public static void reg(Item item) {
		// ModelLoader.setCustomModelResourceLocation(item, 0,
		// new ModelResourceLocation(item.getRegistryName(), "inventory"));
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0,
				new ModelResourceLocation(item.getRegistryName(), "inventory"));
	}

	/** Simply runs RenderItems.reg(Item) for all items in ModItems.allItems */
	public static void register() {
		for (int i = 0; i < ModItems.allItems.length; i++) {
			reg(ModItems.allItems[i]);
		}

	}
}
