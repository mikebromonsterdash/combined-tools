package com.bitbucket.mikebro.combined_tools.items;

import net.minecraft.item.ItemSpade;

/** Class for custom shovels. */
public class ModItemToolSpade extends ItemSpade {

	/** Constructor for a custom shovel. */
	public ModItemToolSpade(String unlocalizedName, ToolMaterial material) {
		super(material);

		this.setUnlocalizedName(unlocalizedName);
		this.setRegistryName(unlocalizedName);
	}
}
