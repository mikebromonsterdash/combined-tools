package com.bitbucket.mikebro.combined_tools.items;

import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fml.common.registry.GameRegistry;

/** initializes, stores and handles all custom Items */
public final class ModItems {

	/* List of all ToolMaterials for custom tools and swords */

	/*
	 * public static ToolMaterial NAME = EnumHelper.addToolMaterial(String name,
	 * int harvestLevel, int maxUses, float miningSpeed, float damage, int
	 * enchantability);
	 */

	public static ToolMaterial doubleWOOD = EnumHelper.addToolMaterial("doubleWOOD", 0, 59 * 2, 2.0F, 0.0F, 15);
	public static ToolMaterial trippleWOOD = EnumHelper.addToolMaterial("trippleWOOD", 0, 59 * 3, 2.0F, 0.0F, 15);

	public static ToolMaterial doubleSTONE = EnumHelper.addToolMaterial("doubleSTONE", 1, 131 * 2, 4.0F, 1.0F, 5);
	public static ToolMaterial trippleSTONE = EnumHelper.addToolMaterial("trippleSTONE", 1, 131 * 3, 4.0F, 1.0F, 5);

	public static ToolMaterial doubleIRON = EnumHelper.addToolMaterial("doubleIRON", 2, 250 * 2, 6.0F, 2.0F, 14);
	public static ToolMaterial trippleIRON = EnumHelper.addToolMaterial("trippleIRON", 2, 250 * 3, 6.0F, 2.0F, 14);

	public static ToolMaterial doubleDIAMOND = EnumHelper.addToolMaterial("doubleDIAMOND", 3, 1561 * 2, 8.0F, 3.0F, 10);
	public static ToolMaterial trippleDIAMOND = EnumHelper.addToolMaterial("trippleDIAMOND", 3, 1561 * 3, 8.0F, 3.0F,
			10);

	public static ToolMaterial doubleGOLD = EnumHelper.addToolMaterial("doubleGOLD", 0, 32 * 2, 12.0F, 0.0F, 22);
	public static ToolMaterial trippleGOLD = EnumHelper.addToolMaterial("trippleGOLD", 0, 32 * 3, 12.0F, 0.0F, 22);

	/*
	 * Extra information:
	 * 
	 * "harvestLevel": for pickaxes; defines which blocks can be mined 0(=wood),
	 * 1(=stone), 2(=iron), 3(=diamond);
	 * 
	 * "maxUses": amount of uses before tool breaks
	 * 
	 * "miningSpeed": is multiplied with default (using ones hand only)
	 * mining/block breaking speed
	 * 
	 * "damage": base damage this material causes on hit, is added to the damage
	 * of some tools
	 * 
	 * "enchantability": something like a probability to get good/better
	 * enchantments
	 */

	/*
	 * List of all custom tools and swords.
	 * 
	 * Refer to com.bitbucket.mikebro.modid.items for all the different item
	 * types.
	 */
	public static Item woodenAxehoe = new ModItemToolAxehoe("woodenAxehoe", doubleWOOD);
	public static Item stoneAxehoe = new ModItemToolAxehoe("stoneAxehoe", doubleSTONE);
	public static Item ironAxehoe = new ModItemToolAxehoe("ironAxehoe", doubleIRON);
	public static Item diamondAxehoe = new ModItemToolAxehoe("diamondAxehoe", doubleDIAMOND);
	public static Item goldAxehoe = new ModItemToolAxehoe("goldAxehoe", doubleGOLD);

	public static Item woodenAxepick = new ModItemToolAxepick("woodenAxepick", doubleWOOD);
	public static Item stoneAxepick = new ModItemToolAxepick("stoneAxepick", doubleSTONE);
	public static Item ironAxepick = new ModItemToolAxepick("ironAxepick", doubleIRON);
	public static Item diamondAxepick = new ModItemToolAxepick("diamondAxepick", doubleDIAMOND);
	public static Item goldAxepick = new ModItemToolAxepick("goldAxepick", doubleGOLD);

	public static Item woodenAxeshovel = new ModItemToolAxeshovel("woodenAxeshovel", doubleWOOD);
	public static Item stoneAxeshovel = new ModItemToolAxeshovel("stoneAxeshovel", doubleSTONE);
	public static Item ironAxeshovel = new ModItemToolAxeshovel("ironAxeshovel", doubleIRON);
	public static Item diamondAxeshovel = new ModItemToolAxeshovel("diamondAxeshovel", doubleDIAMOND);
	public static Item goldAxeshovel = new ModItemToolAxeshovel("goldAxeshovel", doubleGOLD);

	public static Item woodenGatherer = new ModItemToolGatherer("woodenGatherer", trippleWOOD);
	public static Item stoneGatherer = new ModItemToolGatherer("stoneGatherer", trippleSTONE);
	public static Item ironGatherer = new ModItemToolGatherer("ironGatherer", trippleIRON);
	public static Item diamondGatherer = new ModItemToolGatherer("diamondGatherer", trippleDIAMOND);
	public static Item goldGatherer = new ModItemToolGatherer("goldGatherer", trippleGOLD);

	public static Item woodenPickhoe = new ModItemToolPickhoe("woodenPickhoe", doubleWOOD);
	public static Item stonePickhoe = new ModItemToolPickhoe("stonePickhoe", doubleSTONE);
	public static Item ironPickhoe = new ModItemToolPickhoe("ironPickhoe", doubleIRON);
	public static Item diamondPickhoe = new ModItemToolPickhoe("diamondPickhoe", doubleDIAMOND);
	public static Item goldPickhoe = new ModItemToolPickhoe("goldPickhoe", doubleGOLD);

	public static Item woodenPickshovel = new ModItemToolPickshovel("woodenPickshovel", doubleWOOD);
	public static Item stonePickshovel = new ModItemToolPickshovel("stonePickshovel", doubleSTONE);
	public static Item ironPickshovel = new ModItemToolPickshovel("ironPickshovel", doubleIRON);
	public static Item diamondPickshovel = new ModItemToolPickshovel("diamondPickshovel", doubleDIAMOND);
	public static Item goldPickshovel = new ModItemToolPickshovel("goldPickshovel", doubleGOLD);

	public static Item testShovelhoe = new NOTUSED_ModItemToolShovelhoe("testShovelHoe", ToolMaterial.STONE);
	/**
	 * Array; contains all custom tools, weapons and items in general.
	 */
	public static Item[] allItems = { woodenAxehoe, stoneAxehoe, ironAxehoe, diamondAxehoe, goldAxehoe,

			woodenAxepick, stoneAxepick, ironAxepick, diamondAxepick, goldAxepick,

			woodenAxeshovel, stoneAxeshovel, ironAxeshovel, diamondAxeshovel, goldAxeshovel,

			woodenGatherer, stoneGatherer, ironGatherer, diamondGatherer, goldGatherer,

			woodenPickhoe, stonePickhoe, ironPickhoe, diamondPickhoe, goldPickhoe,

			woodenPickshovel, stonePickshovel, ironPickshovel, diamondPickshovel, goldPickshovel,

			 };

	/* --------------------------------------------------------- */

	/** Registers all items in the array allItems */
	public static void createItems() {
		for (int i = 0; i < allItems.length; i++)
			GameRegistry.registerItem(allItems[i]);
	}
}
