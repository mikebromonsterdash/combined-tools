package com.bitbucket.mikebro.combined_tools.items;

import net.minecraft.item.ItemAxe;

/** Class for custom axes */
public class ModItemToolAxe extends ItemAxe {

	/**
	 * Constructor for a custom axe.
	 */
	protected ModItemToolAxe(String unlocalizedName, ToolMaterial material) {
		super(material);
		this.setUnlocalizedName(unlocalizedName);
		this.setRegistryName(unlocalizedName);
	} 
}
