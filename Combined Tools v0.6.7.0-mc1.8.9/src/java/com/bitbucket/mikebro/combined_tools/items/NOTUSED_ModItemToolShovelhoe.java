package com.bitbucket.mikebro.combined_tools.items;

import net.minecraft.item.ItemSpade;

// TO DO: decide whether to use Shovel or Hoe behavior in onItemUse;
//		  decide whether it would be possible/sensible to use both
/** Class for custom "Shovelhoes", a combination of a spade and hoe. */
public class NOTUSED_ModItemToolShovelhoe extends ItemSpade {

	protected NOTUSED_ModItemToolShovelhoe(String unlocalizedName, ToolMaterial material) {
		super(material);

		this.setUnlocalizedName(unlocalizedName);
		this.setRegistryName(unlocalizedName);

	}
}
