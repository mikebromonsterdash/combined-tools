package com.bitbucket.mikebro.combined_tools.items;

import java.util.Set;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;

/**
 * Class for custom "Dirtdigger's Tridents", a combination of a pickaxe, spade
 * and hoe
 */
public class NOTUSED_ModItemToolDirty extends ModItemToolPickhoe {

	protected NOTUSED_ModItemToolDirty(String unlocalizedName, ToolMaterial material) {
		super(unlocalizedName, material);
	}

	@Override
	public Set<String> getToolClasses(ItemStack stack) {
		return ImmutableSet.of("pickaxe", "spade", "hoe");
	}

	private static Set<Block> effectiveAgainst = Sets
			.newHashSet(new Block[] { Blocks.clay, Blocks.dirt, Blocks.farmland, Blocks.grass, Blocks.gravel,
					Blocks.mycelium, Blocks.sand, Blocks.snow, Blocks.snow_layer, Blocks.soul_sand });

}
