package com.bitbucket.mikebro.combined_tools.items;

/**
 * Class for custom "Strage Tridents", a combination of an axe, pickaxe and hoe.
 */
// this is a difficult to visualize tool; planning on dropping it
public class NOTUSED_ModItemToolStrange extends ModItemToolAxepick {

	/**
	 * Constructor for a custom Strage Trident. [damage] determines damage done
	 * by hitting mobs (is added to barehand damage, which is 1.0F). [speed]
	 * determines attack speed (is usually negative and is added to barehand
	 * speed, which is 4.0F).
	 */
	protected NOTUSED_ModItemToolStrange(String unlocalizedName, ToolMaterial material, float damage, float speed) {
		super(unlocalizedName, material);
	}
}
