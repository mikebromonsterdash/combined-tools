package com.bitbucket.mikebro.combined_tools.items;

import java.util.Set;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;

/**
 * Class for custom "Forester's Tridents", a combination of an axe, spade and
 * hoe.
 */
public class NOTUSED_ModItemToolForester extends ModItemToolAxehoe {

	protected NOTUSED_ModItemToolForester(String unlocalizedName, ToolMaterial material) {
		super(unlocalizedName, material);
	}

	@Override
	public Set<String> getToolClasses(ItemStack stack) {
		return ImmutableSet.of("axe", "spade", "hoe");
	}

	private static Set<Block> effectiveAgainst = Sets
			.newHashSet(new Block[] { Blocks.clay, Blocks.dirt, Blocks.farmland, Blocks.grass, Blocks.gravel,
					Blocks.mycelium, Blocks.sand, Blocks.snow, Blocks.snow_layer, Blocks.soul_sand });
}
