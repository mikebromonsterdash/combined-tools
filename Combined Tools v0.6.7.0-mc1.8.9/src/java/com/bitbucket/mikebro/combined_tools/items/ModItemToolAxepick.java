package com.bitbucket.mikebro.combined_tools.items;

import java.util.Set;

import com.google.common.collect.Sets;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;

/** Class for custom "Axepicks", a combination of an axe and pickaxe */
public class ModItemToolAxepick extends ModItemToolPickaxe {

	public static Set<Block> effectiveAgainst = Sets
			.newHashSet(new Block[] { Blocks.planks, Blocks.bookshelf, Blocks.log, Blocks.log2, Blocks.chest,
					Blocks.pumpkin, Blocks.lit_pumpkin, Blocks.melon_block, Blocks.ladder });

	/**
	 * Constructor for a custom Axepick.
	 */
	protected ModItemToolAxepick(String unlocalizedName, ToolMaterial material) {
		super(unlocalizedName, material);
	}

	@Override
	public float getStrVsBlock(ItemStack stack, Block block) {
		if (block.getMaterial() == Material.wood || block.getMaterial() == Material.plants
				|| block.getMaterial() == Material.vine)
			return this.efficiencyOnProperMaterial;

		return effectiveAgainst.contains(block) ? this.efficiencyOnProperMaterial : super.getStrVsBlock(stack, block);
	}

}
