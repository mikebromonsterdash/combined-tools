package com.bitbucket.mikebro.combined_tools.items;

import java.util.Set;

import com.google.common.collect.Sets;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;

/**
 * Class for custom "Gatherer's Tridents", a combination of an axe, pickaxe and
 * spade.
 */
public class ModItemToolGatherer extends ModItemToolAxepick {

	private static Set<Block> effectiveAgainst = Sets
			.newHashSet(new Block[] { Blocks.clay, Blocks.dirt, Blocks.farmland, Blocks.grass, Blocks.gravel,
					Blocks.mycelium, Blocks.sand, Blocks.snow, Blocks.snow_layer, Blocks.soul_sand });

	/**
	 * Constructor for a custom Gatherer's Trident.
	 */
	protected ModItemToolGatherer(String unlocalizedName, ToolMaterial material) {

		super(unlocalizedName, material);
	}

	/**
	 * Check whether this Item can harvest the given Block
	 */
	@Override
	public boolean canHarvestBlock(Block block) {
		return effectiveAgainst.contains(block) ? true : super.canHarvestBlock(block);
	}

	@Override
	public float getStrVsBlock(ItemStack stack, Block block) {
		return effectiveAgainst.contains(block) ? this.efficiencyOnProperMaterial : super.getStrVsBlock(stack, block);
	}
}
