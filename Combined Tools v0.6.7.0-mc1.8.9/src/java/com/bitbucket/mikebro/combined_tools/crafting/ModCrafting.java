package com.bitbucket.mikebro.combined_tools.crafting;

import com.bitbucket.mikebro.combined_tools.items.ModItems;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModCrafting {

	public static void initCrafting() {
		GameRegistry.addRecipe(new ItemStack(ModItems.woodenAxehoe), "AWB", " I ", " I ", 'A', Items.wooden_axe, 'B',
				Items.wooden_hoe, 'W', Blocks.planks, 'I', Items.stick);
		GameRegistry.addRecipe(new ItemStack(ModItems.stoneAxehoe), "AWB", " I ", " I ", 'A', Items.stone_axe, 'B',
				Items.stone_hoe, 'W', Blocks.cobblestone, 'I', Items.stick);
		GameRegistry.addRecipe(new ItemStack(ModItems.ironAxehoe), "AWB", " I ", " I ", 'A', Items.iron_axe, 'B',
				Items.iron_hoe, 'W', Items.iron_ingot, 'I', Items.stick);
		GameRegistry.addRecipe(new ItemStack(ModItems.diamondAxehoe), "AWB", " I ", " I ", 'A', Items.diamond_axe, 'B',
				Items.diamond_hoe, 'W', Items.diamond, 'I', Items.stick);
		GameRegistry.addRecipe(new ItemStack(ModItems.goldAxehoe), "AWB", " I ", " I ", 'A', Items.golden_axe, 'B',
				Items.golden_hoe, 'W', Items.gold_ingot, 'I', Items.stick);

		GameRegistry.addRecipe(new ItemStack(ModItems.woodenAxepick), "AIB", " I ", " I ", 'A', Items.wooden_axe, 'B',
				Items.wooden_pickaxe, 'I', Items.stick);
		GameRegistry.addRecipe(new ItemStack(ModItems.stoneAxepick), "AIB", " I ", " I ", 'A', Items.iron_axe, 'B',
				Items.stone_pickaxe, 'I', Items.stick);
		GameRegistry.addRecipe(new ItemStack(ModItems.ironAxepick), "AIB", " I ", " I ", 'A', Items.iron_axe, 'B',
				Items.iron_pickaxe, 'I', Items.stick);
		GameRegistry.addRecipe(new ItemStack(ModItems.diamondAxepick), "AIB", " I ", " I ", 'A', Items.diamond_axe, 'B',
				Items.diamond_pickaxe, 'I', Items.stick);
		GameRegistry.addRecipe(new ItemStack(ModItems.goldAxepick), "AIB", " I ", " I ", 'A', Items.golden_axe, 'B',
				Items.golden_pickaxe, 'I', Items.stick);

		GameRegistry.addRecipe(new ItemStack(ModItems.woodenAxeshovel), "AWB", " W ", " I ", 'A', Items.wooden_axe, 'B',
				Items.wooden_shovel, 'W', Blocks.cobblestone, 'I', Items.stick);
		GameRegistry.addRecipe(new ItemStack(ModItems.stoneAxeshovel), "AWB", " W ", " I ", 'A', Items.stone_axe, 'B',
				Items.stone_shovel, 'W', Blocks.cobblestone, 'I', Items.stick);
		GameRegistry.addRecipe(new ItemStack(ModItems.ironAxeshovel), "AWB", " W ", " I ", 'A', Items.iron_axe, 'B',
				Items.iron_shovel, 'W', Items.iron_ingot, 'I', Items.stick);
		GameRegistry.addRecipe(new ItemStack(ModItems.diamondAxeshovel), "AWB", " W ", " I ", 'A', Items.diamond_axe,
				'B', Items.diamond_shovel, 'W', Items.diamond, 'I', Items.stick);
		GameRegistry.addRecipe(new ItemStack(ModItems.goldAxeshovel), "AWB", " W ", " I ", 'A', Items.golden_axe, 'B',
				Items.golden_shovel, 'W', Items.gold_ingot, 'I', Items.stick);

		GameRegistry.addRecipe(new ItemStack(ModItems.woodenGatherer), "ABC", "WIW", " I ", 'A', Items.wooden_axe, 'B',
				Items.wooden_pickaxe, 'C', Items.wooden_shovel, 'W', Blocks.planks, 'I', Items.stick);
		GameRegistry.addRecipe(new ItemStack(ModItems.stoneGatherer), "ABC", "WIW", " I ", 'A', Items.stone_axe, 'B',
				Items.stone_pickaxe, 'C', Items.stone_shovel, 'W', Blocks.cobblestone, 'I', Items.stick);
		GameRegistry.addRecipe(new ItemStack(ModItems.ironGatherer), "ABC", "WIW", " I ", 'A', Items.iron_axe, 'B',
				Items.iron_pickaxe, 'C', Items.iron_shovel, 'W', Items.iron_ingot, 'I', Items.stick);
		GameRegistry.addRecipe(new ItemStack(ModItems.diamondGatherer), "ABC", "WIW", " I ", 'A', Items.diamond_axe,
				'B', Items.diamond_pickaxe, 'C', Items.diamond_shovel, 'W', Items.diamond, 'I', Items.stick);
		GameRegistry.addRecipe(new ItemStack(ModItems.goldGatherer), "ABC", "WIW", " I ", 'A', Items.golden_axe, 'B',
				Items.golden_pickaxe, 'C', Items.golden_shovel, 'W', Items.gold_ingot, 'I', Items.stick);

		GameRegistry.addRecipe(new ItemStack(ModItems.woodenGatherer), "ABC", "WIW", " I ", 'A', Items.wooden_pickaxe,
				'B', Items.wooden_axe, 'C', Items.wooden_shovel, 'W', Blocks.planks, 'I', Items.stick);
		GameRegistry.addRecipe(new ItemStack(ModItems.stoneGatherer), "ABC", "WIW", " I ", 'A', Items.stone_pickaxe,
				'B', Items.stone_axe, 'C', Items.stone_shovel, 'W', Blocks.cobblestone, 'I', Items.stick);
		GameRegistry.addRecipe(new ItemStack(ModItems.ironGatherer), "ABC", "WIW", " I ", 'A', Items.iron_pickaxe, 'B',
				Items.iron_axe, 'C', Items.iron_shovel, 'W', Items.iron_ingot, 'I', Items.stick);
		GameRegistry.addRecipe(new ItemStack(ModItems.diamondGatherer), "ABC", "WIW", " I ", 'A', Items.diamond_pickaxe,
				'B', Items.diamond_axe, 'C', Items.diamond_shovel, 'W', Items.diamond, 'I', Items.stick);
		GameRegistry.addRecipe(new ItemStack(ModItems.goldGatherer), "ABC", "WIW", " I ", 'A', Items.golden_pickaxe,
				'B', Items.golden_axe, 'C', Items.golden_shovel, 'W', Items.gold_ingot, 'I', Items.stick);

		GameRegistry.addRecipe(new ItemStack(ModItems.woodenGatherer), "ABC", "WIW", " I ", 'A', Items.wooden_axe, 'B',
				Items.wooden_shovel, 'C', Items.wooden_pickaxe, 'W', Blocks.planks, 'I', Items.stick);
		GameRegistry.addRecipe(new ItemStack(ModItems.stoneGatherer), "ABC", "WIW", " I ", 'A', Items.stone_axe, 'B',
				Items.stone_shovel, 'C', Items.stone_pickaxe, 'W', Blocks.cobblestone, 'I', Items.stick);
		GameRegistry.addRecipe(new ItemStack(ModItems.ironGatherer), "ABC", "WIW", " I ", 'A', Items.iron_axe, 'B',
				Items.iron_shovel, 'C', Items.iron_pickaxe, 'W', Items.iron_ingot, 'I', Items.stick);
		GameRegistry.addRecipe(new ItemStack(ModItems.diamondGatherer), "ABC", "WIW", " I ", 'A', Items.diamond_axe,
				'B', Items.diamond_shovel, 'C', Items.diamond_pickaxe, 'W', Items.diamond, 'I', Items.stick);
		GameRegistry.addRecipe(new ItemStack(ModItems.goldGatherer), "ABC", "WIW", " I ", 'A', Items.golden_axe, 'B',
				Items.golden_shovel, 'C', Items.golden_pickaxe, 'W', Items.gold_ingot, 'I', Items.stick);

		GameRegistry.addRecipe(new ItemStack(ModItems.woodenPickhoe), "AWB", " I ", " I ", 'A', Items.wooden_pickaxe,
				'B', Items.wooden_hoe, 'W', Blocks.planks, 'I', Items.stick);
		GameRegistry.addRecipe(new ItemStack(ModItems.stonePickhoe), "AWB", " I ", " I ", 'A', Items.stone_pickaxe, 'B',
				Items.stone_hoe, 'W', Blocks.cobblestone, 'I', Items.stick);
		GameRegistry.addRecipe(new ItemStack(ModItems.ironPickhoe), "AWB", " I ", " I ", 'A', Items.iron_pickaxe, 'B',
				Items.iron_hoe, 'W', Items.iron_ingot, 'I', Items.stick);
		GameRegistry.addRecipe(new ItemStack(ModItems.diamondPickhoe), "AWB", " I ", " I ", 'A', Items.diamond_pickaxe,
				'B', Items.diamond_hoe, 'W', Items.diamond, 'I', Items.stick);
		GameRegistry.addRecipe(new ItemStack(ModItems.goldPickhoe), "AWB", " I ", " I ", 'A', Items.golden_pickaxe, 'B',
				Items.golden_hoe, 'W', Items.gold_ingot, 'I', Items.stick);

		GameRegistry.addRecipe(new ItemStack(ModItems.woodenPickshovel), "AWB", " W ", " I ", 'A', Items.wooden_pickaxe,
				'B', Items.wooden_shovel, 'W', Blocks.planks, 'I', Items.stick);
		GameRegistry.addRecipe(new ItemStack(ModItems.stonePickshovel), "AWB", " W ", " I ", 'A', Items.stone_pickaxe,
				'B', Items.stone_shovel, 'W', Blocks.cobblestone, 'I', Items.stick);
		GameRegistry.addRecipe(new ItemStack(ModItems.ironPickshovel), "AWB", " W ", " I ", 'A', Items.iron_pickaxe,
				'B', Items.iron_shovel, 'W', Items.iron_ingot, 'I', Items.stick);
		GameRegistry.addRecipe(new ItemStack(ModItems.diamondPickshovel), "AWB", " W ", " I ", 'A',
				Items.diamond_pickaxe, 'B', Items.diamond_shovel, 'W', Items.diamond, 'I', Items.stick);
		GameRegistry.addRecipe(new ItemStack(ModItems.goldPickshovel), "AWB", " W ", " I ", 'A', Items.golden_pickaxe,
				'B', Items.golden_shovel, 'W', Items.gold_ingot, 'I', Items.stick);
	}
}
