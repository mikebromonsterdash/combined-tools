package com.bitbucket.mikebro.combined_tools.items;

import net.minecraft.item.ItemPickaxe;

/** Class for custom pickaxes. */
public class ModItemToolPickaxe extends ItemPickaxe {

	/** Constructor for a custom pickaxe. */
	protected ModItemToolPickaxe(String unlocalizedName, ToolMaterial material) {
		super(material);

		this.setUnlocalizedName(unlocalizedName);
		this.setRegistryName(unlocalizedName);
	}
}
