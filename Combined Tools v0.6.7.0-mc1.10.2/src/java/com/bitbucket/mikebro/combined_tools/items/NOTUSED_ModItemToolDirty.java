package com.bitbucket.mikebro.combined_tools.items;

import java.util.Set;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;

/**
 * Class for custom "Dirtdigger's Tridents", a combination of a pickaxe, spade
 * and hoe
 */
public class NOTUSED_ModItemToolDirty extends ModItemToolPickhoe {

	protected NOTUSED_ModItemToolDirty(String unlocalizedName, ToolMaterial material) {
		super(unlocalizedName, material);
	}

	@Override
	public Set<String> getToolClasses(ItemStack stack) {
		return ImmutableSet.of("pickaxe", "spade", "hoe");
	}

	private static Set<Block> effectiveAgainst = Sets.newHashSet(
			new Block[] { Blocks.CLAY, Blocks.DIRT, Blocks.FARMLAND, Blocks.GRASS, Blocks.GRAVEL, Blocks.MYCELIUM,
					Blocks.SAND, Blocks.SNOW, Blocks.SNOW_LAYER, Blocks.SOUL_SAND, Blocks.GRASS_PATH });

}
