package com.bitbucket.mikebro.combined_tools.proxies;

import com.bitbucket.mikebro.combined_tools.client.renderer.RenderItems;

import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

/** stores and executes Client side initialization events */
public class ProxyClient extends ProxyCommon {

	/** Client pre initialization events */
	@Override
	public void preInit(FMLPreInitializationEvent e) {
		super.preInit(e);
	}

	/** Client main initialization events */
	@Override
	public void init(FMLInitializationEvent e) {
		super.init(e);

		RenderItems.register();
	}

	/** Client post initialization events */
	@Override
	public void postInit(FMLPostInitializationEvent e) {
		super.postInit(e);
	}

}
