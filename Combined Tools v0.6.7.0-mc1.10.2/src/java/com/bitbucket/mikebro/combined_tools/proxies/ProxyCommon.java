package com.bitbucket.mikebro.combined_tools.proxies;

import com.bitbucket.mikebro.combined_tools.crafting.ModCrafting;
import com.bitbucket.mikebro.combined_tools.items.ModItems;

import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

/** stores and executes general initialization events (on both Client and Server side) */
public class ProxyCommon {

	/** pre initialization events, like registration of custom items/blocks */
	public void preInit(FMLPreInitializationEvent e) {
		ModItems.createItems();
	}

	/** main initialization events */
	public void init(FMLInitializationEvent e) {

		ModCrafting.initCrafting();
	}

	/** post initialization events */
	public void postInit(FMLPostInitializationEvent e) {

	}
}
