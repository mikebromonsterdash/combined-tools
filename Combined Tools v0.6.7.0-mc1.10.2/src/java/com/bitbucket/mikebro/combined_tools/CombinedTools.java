package com.bitbucket.mikebro.combined_tools;

import com.bitbucket.mikebro.combined_tools.proxies.ProxyCommon;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

//initializes the mod in Forge
@Mod(modid = CombinedToolsReference.ID, name = CombinedToolsReference.NAME, version = CombinedToolsReference.VER)
public class CombinedTools {

	@Instance(CombinedToolsReference.ID)
	public static CombinedTools instance;

	// initializes the proxy used (either Client or Server)
	@SidedProxy(clientSide = "com.bitbucket.mikebro.combined_tools.proxies.ProxyClient", serverSide = "com.bitbucket.mikebro.combined_tools.proxies.ProxyServer")
	public static ProxyCommon proxy;

	// Game pre initialization events, like registration of custom items/blocks
	@EventHandler
	public void preInit(FMLPreInitializationEvent e) {
		proxy.preInit(e);
	}

	// Game main initialization events, like rendering of textures
	@EventHandler
	public void init(FMLInitializationEvent e) {
		proxy.init(e);
	}

	// Game post initialization events
	@EventHandler
	public void postInit(FMLPostInitializationEvent e) {
		proxy.postInit(e);
	}
}
